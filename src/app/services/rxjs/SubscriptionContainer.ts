import {Subscription} from "rxjs";

export class SubscriptionContainer {
  private subscriptions: Subscription[] = [];

  public add(s: Subscription): void {
    this.subscriptions.push(s);
  }

  public clear() {
    this.subscriptions.forEach(s => s.unsubscribe());
  }
}
